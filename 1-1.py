calories = open("inputs/1.txt").read().split('\n')

maxCalories = 0
elfCalories = 0

for calory in calories:
    if calory == '':
        if elfCalories > maxCalories:
            maxCalories = elfCalories
        elfCalories = 0
    else:
        elfCalories += int(calory)

print(f"Max: {maxCalories}")