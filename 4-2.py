assignements = open("inputs/4.txt").read().split('\n')

counter = 0
for assign in assignements:
    assign1, assign2 = assign.split(',')[0], assign.split(',')[1]
    assign11, assign12 = int(assign1.split('-')[0]), int(assign1.split('-')[1])
    assign21, assign22 = int(assign2.split('-')[0]), int(assign2.split('-')[1])

    combi = sorted([[assign11, assign12], [assign21, assign22]])
    print(combi)
    if combi[1][0] <= combi[0][1]:
        counter += 1

print(counter)