chars = open("inputs/6.txt").read()

counter = 4
code = chars[0:4]
for i in range(4,len(chars)+1):
    code += chars[i]
    code = code[1:]
    counter += 1

    if len(set(code)) == len(code):
        print(counter)
        break
