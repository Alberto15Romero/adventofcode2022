games = open("inputs/2.txt").read().split('\n')

def gameResult(one, two):
    if one == 'A':
        if two == 'X':
            return 'draw'
        if two == 'Y':
            return 'win'
        if two == 'Z':
            return 'loose'
    
    if one == 'B':
        if two == 'X':
            return 'loose'
        if two == 'Y':
            return 'draw'
        if two == 'Z':
            return 'win'

    if one == 'C':
        if two == 'X':
            return 'win'
        if two == 'Y':
            return 'loose'
        if two == 'Z':
            return 'draw'

def pointsInRound(one, two):
    points = 0

    if two == 'X':
        points += 1
    if two == 'Y':
        points += 2
    if two == 'Z':
        points += 3
    
    if gameResult(one, two) == 'win':
        points += 6
    elif gameResult(one, two) == 'draw':
        points += 3

    return points

def objective(one, result):

    if one == 'A':
        if result == 'X':
            return 'Z'
        if result == 'Y':
            return 'X'
        if result == 'Z':
            return 'Y'

    if one == 'B':
        if result == 'X':
            return 'X'
        if result == 'Y':
            return 'Y'
        if result == 'Z':
            return 'Z'
    
    if one == 'C':
        if result == 'X':
            return 'Y'
        if result == 'Y':
            return 'Z'
        if result == 'Z':
            return 'X'


total = 0
for game in games:
    total += pointsInRound(game.split(' ')[0], objective(game.split(' ')[0], game.split(' ')[1]))


print(total)