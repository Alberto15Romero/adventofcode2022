movements = open("inputs/5.txt").read().split('\n')

global STACKS 

STACKS = {
    '1':['Q', 'W', 'P', 'S', 'Z', 'R', 'H', 'D'],
    '2':['V', 'B', 'R', 'W', 'Q', 'H', 'F'],
    '3':['C', 'V', 'S', 'H'],
    '4':['H', 'F', 'G'],
    '5':['P', 'G', 'J', 'B', 'Z'],
    '6':['Q', 'T', 'J', 'H', 'W', 'F', 'L'],
    '7':['Z', 'T', 'W', 'D', 'L', 'V', 'J', 'N'],
    '8':['D', 'T', 'Z', 'C', 'J', 'G', 'H', 'F'],
    '9':['W', 'P', 'V', 'M', 'B', 'H']
}

def moveCrane(origin, destination):
    STACKS[destination].append(STACKS[origin].pop())

for move in movements:
    quantity = int(move.split('from')[0].replace('move', '').strip())
    origin = move.split('from')[1][1]
    destination = move.split('from')[1][-1]

    for _ in range(quantity):
        moveCrane(origin, destination)

res = ''
for key in STACKS.keys():
    res += STACKS[key][-1]

print(res)