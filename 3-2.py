import string
items = open("inputs/3.txt").read().split('\n')

ord = dict(list(zip(string.ascii_lowercase, range(1,27))) + list(zip(string.ascii_uppercase, range(27,53))))

def strIntersection(s1, s2):
    out = ""
    for c in s1:
        if c in s2 and not c in out:
            out += c
    return out

prio = 0
counter = 0
for i in range(0, len(items)//3):
    item1, item2, item3 = items[i*3], items[i*3 + 1], items[i*3 + 2]
    inter = strIntersection(strIntersection(item1, item2), item3)

    prio += int(ord[inter])
    print(prio)