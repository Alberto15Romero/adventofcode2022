calories = open("inputs/1.txt").read().split('\n')

elfs = []
elfCalories = 0

for calory in calories:
    if calory == '':
        elfs.append(elfCalories)
        elfCalories = 0
    else:
        elfCalories += int(calory)

elfs = sorted(elfs,reverse=True)
print(f"Sum of top 3 elves: {sum(elfs[0:3])}")