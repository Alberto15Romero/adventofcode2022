import string
items = open("inputs/3.txt").read().split('\n')

ord = dict(list(zip(string.ascii_lowercase, range(1,27))) + list(zip(string.ascii_uppercase, range(27,53))))

def strIntersection(s1, s2):
    out = ""
    for c in s1:
        if c in s2 and not c in out:
            out += c
    return out

prio = 0
for item in items:
    item1, item2  = item[:len(item)//2], item[len(item)//2:]
    prio += int(ord[strIntersection(item1, item2)])

print(prio)