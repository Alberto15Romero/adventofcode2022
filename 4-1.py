assignements = open("inputs/4.txt").read().split('\n')

counter = 0
for assign in assignements:
    assign1, assign2 = assign.split(',')[0], assign.split(',')[1]
    
    assign11, assign12 = int(assign1.split('-')[0]), int(assign1.split('-')[1])
    assign21, assign22 = int(assign2.split('-')[0]), int(assign2.split('-')[1])
    list1 = range(assign11, assign12+1)
    list2 = range(assign21, assign22+1)

    if set(list1).issubset(set(list2)) or set(list2).issubset(set(list1)):
        counter += 1

print(counter)